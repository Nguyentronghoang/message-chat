const WindowsToaster = require('node-notifier').WindowsToaster;
const notifier= new WindowsToaster({
    withFallback:false,
    custoomPath: void 0
});

export default class NotifierMessage {
     
    public SendNotifier(message){
        notifier.notify({
            'title': void 0,
            'message': message,
            'sound':false,
            'icon': '../client/public/assets/icons/default.png', 
            'wait':false,
            "Allow Button Text":"allow",
            "Disallow Button Text":"not",
            id: void 0,
            appId: void 0, 
            remove: void 0,
            install: void 0
        },function(error,response){
            console.log(error,response);
        })
    }
}
 