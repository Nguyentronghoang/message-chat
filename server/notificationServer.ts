var socketIO = require('socket.io');
var socketIOHelper = require('./app/helpers/socketio');
var PORT = process.env.PORT || 8088;
import express from 'express';
var app = express();
var server = app.listen(PORT, function() {
  console.log('Listening on port ' + PORT);
});
var io = socketIO(server);
socketIOHelper.set(io);
var receivers = require('./app/sockets/receivers.server.sockets');
receivers.receivers(io);