// server tạo socket io
import express from 'express';
let app = express();
var http = require('http').Server(app);
var io = require('socket.io')(http);
var bodyParser = require('body-parser');
// Model schemas
var Message = require('./src/models/messageModel');
var User = require('./src/models/userModel');
var Chatroom = require('./src/models/chatroomModel');
var Notification = require('./src/models/notificationModel');
//  controller for socket
import chatController  from'./src/controllers/chatControllers';
import chatroomControllers from'./src/controllers/chatroomControllers';
import NotifierMessage from './notification';
//  import route
var routeNotification = require('./src/routes/routeNotification');
var routeChat = require('./src/routes/routeChat');
var routeUser = require('./src/routes/routeUser');
var routeChatroom = require('./src/routes/routeChatroom');

var mongoose = require('mongoose');
let allowCrossDomain = function(req, res, next) {
   res.header('Access-Control-Allow-Origin', '*');
   res.header('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE');
   res.header('Access-Control-Allow-Headers', 'Content-Type');

   next();
}
mongoose.Promise = global.Promise;

// kết nối cơ sở dữ liệu
mongoose.connect('mongodb://localhost:27017/MessageChat')
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));
app.use(allowCrossDomain);
// route for (req, res) , param app:express()
routeChat(app);
routeUser(app);
routeChatroom(app);
routeNotification(app);


var users = [];
let notifierMsg = new NotifierMessage();
//  tạo một socket  server
io.on('connection', function (socket) {
   var chat = chatController();
   var room= chatroomControllers();
   //tạo chatroom mới
   socket.on('CreatChatroom', async function (data,callback) {
      let users= new Array();
      users.push(data.name);
      const new_chatroom={users:users,name:data.chatroom}
     try{
      let chatroom = await room.creatChatroom(new_chatroom);
      console.log(chatroom); 
      callback(null,chatroom);
   }catch(e){
      callback(e);
      console.log(e);
   }
   });
   console.log('A now connected');
   // lay message
   socket.on('getMessage', async function (data, callback) {
      console.log("start get message");
      console.log(data);
      try {
         var messages = await chat.getMessage(data);
         callback(null,messages);
      
      } catch (err) {
           callback.send(err)
      }
      console.log("end message");

   })
   // tao mot message
   socket.on('msg', function (data) {
      // Send message to everyone
      var message = {
         chatroom: data.chatroom,
         name: data.name,
         message: data.message
      }
       
      chat.creatMessage(message);
      io.sockets.emit('newnotifi',data)
      io.sockets.emit('newmsg', data);
       const notifier= data.name+" send "+data.message+"in"+data.chatroom;// message thông báo
      notifierMsg.SendNotifier(notifier);
       
   })
   socket.on('error', function (err) {
      console.log('received error from client:')
      console.log(err)
      
   })
   socket.on('disconnect', function () {

      console.log('a user disconnect');
   })
});

http.listen(3001, function () {
   
   console.log('listening on localhost:3001');
});