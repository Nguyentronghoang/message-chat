'use strict';
import mongoose = require('mongoose');
var Schema = mongoose.Schema;
const Notification = new Schema({
    createDate: {
        type: Date,
        default: Date.now
    },
    title: {
        type: String,
        default: void 0
    },
    message:{
        type : String,
        default:''
    },
    sound: {
        type:String,
        default: false
    },
    icon:{
        type: String,
        default: 'public/assets/icons/default'
     },
    status:{
        type: Boolean,
        default: false
    }

})
export default  mongoose.model('Notification',Notification);