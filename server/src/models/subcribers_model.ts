import mongoose = require('mongoose');
const Schema = mongoose.Schema;
const SubscriberSchema = new Schema({
    endpoint: String,
    keys: Schema.Types.Mixed,
    createDate:{
        type: Date,
        default: Date.now
    }
});
export default mongoose.model('subscribers',SubscriberSchema,'subscribers');