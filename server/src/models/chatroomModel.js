'use strict';
Object.defineProperty(exports, "__esModule", { value: true });
const mongoose = require("mongoose");
var Schema = mongoose.Schema;
const Chatroom = new Schema({
    name: String,
    users: {
        type: [{
                type: String
            }]
    },
    CreatDate: {
        type: Date,
        default: Date.now
    }
});
exports.default = mongoose.model('Chatroom', Chatroom);
//# sourceMappingURL=chatroomModel.js.map