"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const mongoose = require("mongoose");
const Schema = mongoose.Schema;
const SubscriberSchema = new Schema({
    endpoint: String,
    keys: Schema.Types.Mixed,
    createDate: {
        type: Date,
        default: Date.now
    }
});
exports.default = mongoose.model('subscribers', SubscriberSchema, 'subscribers');
//# sourceMappingURL=subcribers_model.js.map