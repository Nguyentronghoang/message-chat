'use strict'
import mongoose = require('mongoose');
var Schema = mongoose.Schema;
const User= new Schema({
    fistname: String,
    lastname: String,
    name: String,
    img: String,
    pass: String,
    CreatDate:{
        type: Date,
        default: Date.now,
    },
    status:{
        type: [{
            type: String,
            enum: ['admin','vendor','customer'],
            default: ['customer']
        }]
    },
    chatroom:{
        type:[{
            type: String,
            default:[]
        }]
    }    
});
export default mongoose.model('User',User);