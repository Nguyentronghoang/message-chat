//  model message
'use strict';
import mongoose = require('mongoose');
var Schema = mongoose.Schema;
const MessageChat = new Schema({
    chatroom: String,
    Created_date: {
        type: Date,
        default: Date.now
      },
    name: String,
    message: String
});
export default mongoose.model('Message', MessageChat);



