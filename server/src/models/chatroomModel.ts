'use strict'
import mongoose = require('mongoose');
var Schema = mongoose.Schema;
const Chatroom = new Schema({
    name: String,
    users: {
        type:[{
            type: String
        }]
    },
    CreatDate:{
        type: Date,
        default: Date.now
    }
});
export default mongoose.model('Chatroom',Chatroom);
