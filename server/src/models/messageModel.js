//  model message
'use strict';
Object.defineProperty(exports, "__esModule", { value: true });
const mongoose = require("mongoose");
var Schema = mongoose.Schema;
const MessageChat = new Schema({
    chatroom: String,
    Created_date: {
        type: Date,
        default: Date.now
    },
    name: String,
    message: String
});
exports.default = mongoose.model('Message', MessageChat);
//# sourceMappingURL=messageModel.js.map