'use strict';
Object.defineProperty(exports, "__esModule", { value: true });
const mongoose = require("mongoose");
var Schema = mongoose.Schema;
const User = new Schema({
    fistname: String,
    lastname: String,
    name: String,
    img: String,
    pass: String,
    CreatDate: {
        type: Date,
        default: Date.now,
    },
    status: {
        type: [{
                type: String,
                enum: ['admin', 'vendor', 'customer'],
                default: ['customer']
            }]
    },
    chatroom: {
        type: [{
                type: String,
                default: []
            }]
    }
});
exports.default = mongoose.model('User', User);
//# sourceMappingURL=userModel.js.map