'use strict';
Object.defineProperty(exports, "__esModule", { value: true });
const mongoose = require("mongoose");
var Schema = mongoose.Schema;
const Notification = new Schema({
    createDate: {
        type: Date,
        default: Date.now
    },
    title: {
        type: String,
        default: void 0
    },
    message: {
        type: String,
        default: ''
    },
    sound: {
        type: String,
        default: false
    },
    icon: {
        type: String,
        default: 'public/assets/icons/default'
    },
    wait: {
        type: String,
        default: false
    },
    AllowButtonText: {
        type: String,
        default: 'allow'
    },
    DisallowButtonText: {
        type: String,
        default: 'not'
    },
    id: {
        type: String,
        default: void 0
    },
    appID: {
        type: String,
        default: void 0
    },
    removeNoti: {
        type: String,
        default: void 0
    },
    install: {
        type: String,
        default: void 0
    }
});
exports.default = mongoose.model('Notification', Notification);
//# sourceMappingURL=notificationModel.js.map