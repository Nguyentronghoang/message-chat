'use strict';
var mongoose = require('mongoose'), User = mongoose.model('User');
module.exports = function (app) {
    app.get('/users', (req, res) => {
        User.find({}, (err, user) => {
            res.send(user);
        });
    });
    app.get('/user', (req, res) => {
        console.log(req.query.name);
        User.find({ name: req.query.name, pass: req.query.pass }, (err, user) => {
            if (err)
                res.send(err);
            res.send(user);
            console.log(user);
        });
    });
    app.post('/user/login', (req, res) => {
        console.log(req.body);
        User.find({ name: req.body.name }, (err, user) => {
            if (err)
                res.send(err);
            res.json(user);
        });
    });
    app.post('/user', (req, res) => {
        console.log(req.body);
        var user = new User(req.body);
        user.save((err) => {
            if (err)
                res.sendStatus(500);
            // io.emit('User', req.body);
            res.json({ status: "success", user });
        });
    });
};
//# sourceMappingURL=routeUser.js.map