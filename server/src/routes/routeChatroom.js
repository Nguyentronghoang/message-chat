'use strict';
var mongoose = require('mongoose'), Chatroom = mongoose.model('Chatroom');
module.exports = function (app) {
    app.get('/chatrooms', (req, res) => {
        Chatroom.find({}, (err, chatroom) => {
            res.send(chatroom);
        });
    });
    app.post('/chatroom', (req, res) => {
        console.log(req.body);
        var chatroom = new Chatroom(req.body);
        chatroom.save((err) => {
            if (err)
                res.send(err);
            // io.emit('message', req.body);
            res.json({ status: "success", chatroom });
        });
    });
    app.delete('/chatroom/:idChatroom', (req, res) => {
        var id = req.param.id;
        Chatroom.remove({ id_: id }, (err, chatroom) => {
            if (err)
                res.send(err);
            res.json({ status: "delete success", chatroom });
        });
    });
    app.delete('/chatroom', (req, res) => {
        var id = req.query.id;
        Chatroom.remove({ id_: id }, (err, chatroom) => {
            if (err)
                res.send(err);
            res.json({ status: "delete success", chatroom });
        });
    });
};
//# sourceMappingURL=routeChatroom.js.map