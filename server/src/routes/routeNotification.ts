'use strict'
import mongoose= require('mongoose');
var Notification = mongoose.model('Notification');
module.exports= function(app){
    app.get('/notification',(req,res)=>{
        Notification.find({},(err,noti)=>{
            if(err)
               res.send(err);
            else
              res.json(noti);   
        })
    });
    app.get('/notification/:idNoti',(req,res)=>{
        const id= req.param.id;
        Notification.find({id_:id},(err,noti)=>{
            if(err)
               res.send(err);
            else
              res.json(noti);
        })
    });
    app.post('/notification',(req,res)=>{
        var new_noti = new Notification(req.body);
        new_noti.save((err)=>{
            if(err)
              res.send(err)
            res.json({"status": "success",new_noti});  
        })
    })
}