'use strict'
var mongoose = require('mongoose'),
    Message = mongoose.model('Message');
module.exports = function(app){
    app.get('/messages', (req, res) => {
        Message.find({},(err, messages)=> {
          res.send(messages);
        })
      })
     app.get('/chatroom/messages',(req,res)=>{
       Message.find({chatroom:req.query.chatroom},(err,message)=>{
          if(err) res.send(err);
          res.json(message);    
       })
     }) 
      app.post('/message', (req, res) => {
        console.log(req.body);
        var message = new Message(req.body);
        message.save((err) =>{
          if(err)
            res.sendStatus(500);
          // io.emit('message', req.body);
          res.json({status:"success",message});
         
        })
      })

}