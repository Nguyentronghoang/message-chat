//CRUD chatroom
import mongoose = require('mongoose');
import { resolve, reject } from 'bluebird';
import { rejects } from 'assert';
var Chatroom = mongoose.model('Chatroom');
export default function () {
    // get list Chatroom
    function getListChatroom() {
        return new Promise((resolve, reject) => {
            Chatroom.find({}, (err, Chatrooms) => {
                if (err)
                    reject(err);
                else
                    resolve(Chatrooms);

            });
        });
    }
    // get a Chatroom, paragram name
    function getChatroom(name) {
        return new Promise((resolve, reject) => {
            Chatroom.find({ name: name }, (err, Chatroom) => {
                if (err)
                    reject(err);
                else
                    resolve(Chatroom);
            });
        });
    }
    function creatChatroom(data) {
        var new_Chatroom = new Chatroom(data);
        return new Promise((resolve, reject) => {
            new_Chatroom.save((err, Chatroom) => {
                if (err)
                    reject(err);
                else (Chatroom)
                resolve(Chatroom);
            });
        })
    }
    function updateChatroom(data) {
        return new Promise((resolve, reject) => {
            Chatroom.findOneAndUpdate({ name: data.name }, data, { new: true }, (err, Chatroom) => {
                if (err)
                    reject(err);
                else
                    resolve("success");
            });
        });
    }
    function removeChatroom(name) {
        return new Promise((resolve, reject) => {
            Chatroom.remove({ name: name }, (err, Chatroom) => {
                if (err)
                    reject(err)
                else
                    resolve("success");
            });
        });
    }
    return {
        getListChatroom,
        getChatroom,
        creatChatroom,
        updateChatroom,
        removeChatroom
    }
}