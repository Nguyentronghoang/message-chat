// CRUD message
'use strict';
Object.defineProperty(exports, "__esModule", { value: true });
const mongoose = require("mongoose");
var Message = mongoose.model('Message');
function default_1() {
    function getMessage(chatroom) {
        var query = {
            skin: 30
        };
        //query.limit=10;
        return new Promise((resolve, reject) => {
            Message.find({ chatroom: chatroom }, {}, query, (err, messages) => {
                if (err)
                    reject(err);
                else
                    resolve(messages);
            });
        });
    }
    function creatMessage(data) {
        var message = new Message(data);
        message.save((err) => {
            if (err)
                return err;
            // io.emit('message', req.body);
            return ({ status: "success", message });
        });
    }
    return {
        getMessage,
        creatMessage
    };
}
exports.default = default_1;
//# sourceMappingURL=chatControllers.js.map