// CRUD message
'use strict'
import mongoose = require('mongoose');
  var  Message = mongoose.model('Message');
export default function () {
    function getMessage(chatroom) {
        var query={
            skin: 30
        };
        
        //query.limit=10;
        return new Promise((resolve, reject) => {
            Message.find({chatroom:chatroom},{},query, (err, messages) => {
                if (err)
                    reject(err);
                else
                    resolve(messages);
            })
        })
    }
    function creatMessage(data) {
        var message = new Message(data);
        message.save((err) => {
            if (err)
                return err;
            // io.emit('message', req.body);
            return ({ status: "success", message });

        })
    }
    return {
        getMessage,
        creatMessage
    }
}