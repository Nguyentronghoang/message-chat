//CRUD user
import mongoose = require('mongoose');
import { resolve, reject } from 'bluebird';
import { rejects } from 'assert';
var User = mongoose.model('User');
export default function () {
    // get list user
    function getListUser() {
        return new Promise((resolve, reject) => {
            User.find({}, (err, users) => {
                if (err)
                    reject(err);
                else
                    resolve(users);

            });
        });
    }
    // get a user, paragram name
    function getUser(name) {
        return new Promise((resolve, reject) => {
            User.find({ name: name }, (err, user) => {
                if (err)
                    reject(err);
                else
                    resolve(user);
            });
        });
    }
    function creatUser(data) {
        var new_user = new User(data);
        return new Promise((resolve, reject) => {
            new_user.save((err, user) => {
                if (err)
                    reject(err);
                else (user)
                resolve(user);
            });
        })
    }
    function updateUser(data) {
        return new Promise((resolve, reject) => {
            User.findOneAndUpdate({ name: data.name }, data, { new: true }, (err, user) => {
                if (err)
                    reject(err);
                else
                    resolve("success");
            });
        });
    }
    function removeUser(name) {
        return new Promise((resolve, reject) => {
            User.remove({ name: name }, (err, user) => {
                if (err)
                    reject(err)
                else
                    resolve("success");
            });
        });
    }
    return {
        getListUser,
        getUser,
        creatUser,
        updateUser,
        removeUser
    }
}