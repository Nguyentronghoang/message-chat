"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
// server tạo socket io
const express_1 = __importDefault(require("express"));
let app = express_1.default();
var http = require('http').Server(app);
var io = require('socket.io')(http);
var bodyParser = require('body-parser');
// Model schemas
var Message = require('./src/models/messageModel');
var User = require('./src/models/userModel');
var Chatroom = require('./src/models/chatroomModel');
var Notification = require('./src/models/notificationModel');
//  controller for socket
const chatControllers_1 = __importDefault(require("./src/controllers/chatControllers"));
const chatroomControllers_1 = __importDefault(require("./src/controllers/chatroomControllers"));
const notification_1 = __importDefault(require("./notification"));
//  import route
var routeNotification = require('./src/routes/routeNotification');
var routeChat = require('./src/routes/routeChat');
var routeUser = require('./src/routes/routeUser');
var routeChatroom = require('./src/routes/routeChatroom');
var mongoose = require('mongoose');
let allowCrossDomain = function (req, res, next) {
    res.header('Access-Control-Allow-Origin', '*');
    res.header('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE');
    res.header('Access-Control-Allow-Headers', 'Content-Type');
    next();
};
mongoose.Promise = global.Promise;
// kết nối cơ sở dữ liệu
mongoose.connect('mongodb://localhost:27017/MessageChat');
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));
app.use(allowCrossDomain);
// route for (req, res) , param app:express()
routeChat(app);
routeUser(app);
routeChatroom(app);
routeNotification(app);
var users = [];
let notifierMsg = new notification_1.default();
//  tạo một socket  server
io.on('connection', function (socket) {
    var chat = chatControllers_1.default();
    var room = chatroomControllers_1.default();
    //tạo chatroom mới
    socket.on('CreatChatroom', function (data, callback) {
        return __awaiter(this, void 0, void 0, function* () {
            let users = new Array();
            users.push(data.name);
            const new_chatroom = { users: users, name: data.chatroom };
            try {
                let chatroom = yield room.creatChatroom(new_chatroom);
                console.log(chatroom);
                callback(null, chatroom);
            }
            catch (e) {
                callback(e);
                console.log(e);
            }
        });
    });
    console.log('A now connected');
    // lay message
    socket.on('getMessage', function (data, callback) {
        return __awaiter(this, void 0, void 0, function* () {
            console.log("start get message");
            console.log(data);
            try {
                var messages = yield chat.getMessage(data);
                callback(null, messages);
            }
            catch (err) {
                callback.send(err);
            }
            console.log("end message");
        });
    });
    // tao mot message
    socket.on('msg', function (data) {
        // Send message to everyone
        var message = {
            chatroom: data.chatroom,
            name: data.name,
            message: data.message
        };
        chat.creatMessage(message);
        io.sockets.emit('newnotifi', data);
        io.sockets.emit('newmsg', data);
        const notifier = data.name + " send " + data.message + "in" + data.chatroom; // message thông báo
        notifierMsg.SendNotifier(notifier);
    });
    socket.on('error', function (err) {
        console.log('received error from client:');
        console.log(err);
    });
    socket.on('disconnect', function () {
        console.log('a user disconnect');
    });
});
http.listen(3001, function () {
    console.log('listening on localhost:3001');
});
//# sourceMappingURL=server.js.map