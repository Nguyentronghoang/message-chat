"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
var socketIO = require('socket.io');
var socketIOHelper = require('./app/helpers/socketio');
var PORT = process.env.PORT || 8088;
const express_1 = __importDefault(require("express"));
var app = express_1.default();
var server = app.listen(PORT, function () {
    console.log('Listening on port ' + PORT);
});
var io = socketIO(server);
socketIOHelper.set(io);
var receivers = require('./app/sockets/receivers.server.sockets');
receivers.receivers(io);
//# sourceMappingURL=notificationServer.js.map