
import * as io from 'socket.io-client'
import * as $ from 'jquery';
const socket = io.connect("http://localhost:3001");
export default class SocketIO {

    private name: string;

    private chatroom: string;
   // hàm khởi tạo param component: React.Conponent
    constructor(component: any) {
        const self = this;
        socket.on('newmsg', function (data: any) {

            if (self.name && self.chatroom === data.chatroom) {
                const message = {
                    name: data.name,
                    message: data.message
                }
                // them message nhận được khi gửi
                component.setState({
                    messages: [...component.state.messages, message]
                })

            }
        });
        socket.on('newnotifi', function (data: any) {
            console.log("notification received")
            $('.list-notification').append('<div><a href="Messagelist" ><b>' +
                data.name + '</b>: ' + 'đã gửi tin nhắn trong' + data.chatroom + '</a></div>');

        });
        // tự động lấy notification khi khởi động
        socket.on('getNotification', function (data: any) {

        })

    }

    public CreatChatroom(data: any, cb: (err: any, data: any) => void) {
        console.log("da duoc goi")
        socket.emit('CreatChatroom', data, cb);
        console.log("da vao")

    }

    // load message in chat room parameter data:string, cb: callback
    public getMessage(data: any, cb: any) {
        console.log('get message callded')
        socket.emit('getMessage', data, cb);

    }
    // join chat room, parameter chatroom: string, username: string
    public joinChatRoom(c: string, n: string) {
        this.name = n;
        this.chatroom = c;

    }

    // send message, parameter  message: string
    public sendMessage(msg: string) {
        console.log("day la nguoi chat" + this.name);

        const data = { chatroom: this.chatroom, message: msg, name: this.name }
        if (msg) {
            socket.emit('msg', data);
        }
    }
    public updateStatus(){
        
    }

}