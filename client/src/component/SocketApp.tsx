import * as React from 'react'
import SocketIO from '../socket.io';
import Chatroom from './chatroom/Chatroom';
import SendMessageForm from './SendMessageFrom';
// import Message from './Message';
import MessageList from './MessageList'
import * as $ from 'jquery';
import './socketapp.css';


export interface ISoketAppProps { }

export interface ISocketAppStates {
    loginStatus: boolean;
    messages: any[];
    roomId: string;
    listflow: string[];
    username: string;
    numberNoti:number;
    user: {
        name: string;
        firstname: string;
        lastname: string;
        chatrooms: string[];
        img: string;
        status: string;
    }
}
export default class SocketApp extends React.Component<ISoketAppProps, ISocketAppStates> {

    private socketIO: SocketIO;

    constructor(props: ISoketAppProps) {
        super(props);
        const self= this;
        this.socketIO = new SocketIO(self);
        this.state = {
            numberNoti: 0,
            loginStatus: true,
            messages: [],
            roomId: '',
            listflow: ['hoang', 'hung', 'vu'],
            username: '',
            user: {
                name: 'HoangGI',
                firstname: 'Nguyễn Trọng',
                lastname: 'Hoàng',
                chatrooms: ["room1", "room2", "room3", "room8"],
                img: '/assets/images/default.jpg',
                status: 'admin'
            }
        }
        this.login = this.login.bind(this);
        this.CreatChatroom = this.CreatChatroom.bind(this);
        this.sendMessage = this.sendMessage.bind(this);
        this.getMessages = this.getMessages.bind(this);
        this.joinChatroom = this.joinChatroom.bind(this);

    }
    public login() {
        const name = ($('.username') as any).val();
        this.setState({
            username: name,
            loginStatus: true
        });
    }
    public getMessages(chatroom: string) {
        this.socketIO.getMessage(chatroom, (err: any, value: any) => {
            if (err) console.log(err);

            this.setState({ messages: value });
            console.log(this.state.messages);
        });
    }
    public CreatChatroom() {
        let chatroom = '';

        chatroom = ($('#new-chatroom') as any).val();


        if (chatroom === null || chatroom.trim() === '') {
            console.log("test data");
            $('#error-container').html('chatroom null');
        }
        else {
            this.socketIO.CreatChatroom({ name: this.state.user.name, chatroom: chatroom }, (err: any, value: any) => {
                if (err) {
                    console.log(err);
                } else {
                    console.log(value);
                    const userUpdate = this.state.user;
                    userUpdate.chatrooms.push(chatroom);
                    this.setState({
                        roomId: chatroom,
                        user: userUpdate
                    });
                    this.joinChatroom(chatroom);
                }
            });

        }
    }
    public sendMessage(msg: string) {
        console.log("send message run")
        console.log('vao send message');
        if (!(msg === null || msg.trim() === '')) {
            this.socketIO.sendMessage(msg);
            $('#message').val('');

        }

    }
    public joinChatroom(chatroom: string) {
        console.log("join chatroom run");
        const name = this.state.user.name;
        console.log(chatroom);
        this.getMessages(chatroom);
        this.socketIO.joinChatRoom(chatroom, name);
        this.setState({ roomId: chatroom });
    }
    public watch(){
        this.socketIO.updateStatus();
    }
    public render() {
        if (!this.state.loginStatus) {
            return (
                <div>
                    <form className="form-login">
                        <input className="username" type="text" placeholder="enter your user" />
                        <input className="password" type="text" placeholder="enter your password"/>
                        <button className="login" onClick={this.login}>Login</button>
                    </form>
                </div>
            )
        } else {
            return (
                <div>
                    <div id="user-infomation-container">
                        <div><img className="img-responsive" src={this.state.user.img} alt="..." /></div>
                        <div className="info">
                            <ul className="list-info">
                                <li><p>firstname:{this.state.user.firstname} </p></li>
                                <li><p>lastname:{this.state.user.lastname} </p></li>
                                <li><p>status:{this.state.user.status} </p></li>
                                <li><p>username:{this.state.username} </p></li>
                            </ul>
                        </div>
                        <div>
                            <a className="notificcation" href="#">notification: {this.state.numberNoti}</a>
                            <div className="list-notification"> show notification onlick{watch}</div>
                        </div>
                        
                        <div id="roomlist">{this.state.user.chatrooms.map((chatroom, i) => <Chatroom key={i} chatroom={chatroom} join={this.joinChatroom} />)}</div>
                    </div>
                    <div className="main-content">
                        <div id="container">
                            <div id="error-container" />
                            <input id="new-chatroom" type="text" placeholder="Enter your chatroom!" /><br />
                            <button type="button" name="button" onClick={this.CreatChatroom}>Creat chat room!</button>

                        </div>

                        <MessageList messages={this.state.messages} roomId={this.state.roomId} />

                        <SendMessageForm sendMessage={this.sendMessage} disabled={!this.state.roomId} />
                    </div>
                </div>
            )
        }
    }
}
