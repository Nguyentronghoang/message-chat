import * as React from 'react';
export interface ISendMessageProps{
  sendMessage: (message:string)=>void;
  disabled: boolean;
}

export interface ISendMessagStates{
    message: string;
}
export default class Message extends React.Component<ISendMessageProps,ISendMessagStates> {
              constructor(props:ISendMessageProps){
                  super(props);
                  this.state={
                      message:''
                  }
                  this.handleChange= this.handleChange.bind(this);
                  this.handleSubmit= this.handleSubmit.bind(this);
              }
              public handleChange(e:any){
                  this.setState(
                      {message:e.target.value}
                      )
              }
              public handleSubmit(e:any){
                  e.preventDefault();
                  this.props.sendMessage(this.state.message);
                  this.setState({message:''});

              }
    public   render() {
        return (
            <form onSubmit={this.handleSubmit}
                  className="send-message-form"
            >
            <input
            disabled={this.props.disabled} 
            onChange={this.handleChange}
            value={this.state.message}
            placeholder="Enter your message" type="text"/>
            <button>Send</button>
            </form>
        )
    }
}