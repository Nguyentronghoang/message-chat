
import * as React from 'react';
import Message from './Message';
interface IMessageListProps {
    roomId: string;
    messages: any[];
    
}
interface IMessageListStates { }
export default class MessageList extends React.Component<IMessageListProps, IMessageListStates> {
    constructor(props: IMessageListProps) {
        super(props);
    }


    public render() {
        if (!this.props.roomId) {
            return (
                <div className="message-list">
                    <div className="join-room"/>
                </div>
            )
        } else {
            return (
                <div className="message-list">
                <h1>chatrom:{this.props.roomId}</h1>
              

                    {this.props.messages.map((message, index) => {
                      return(  
                        <Message key={index} username={message.name} text={message.message} />)
                    }
                    )}
                </div>
            )
        }
    }
}