import * as React from 'react';
import * as $ from 'jquery';
import * as request from 'request';
export interface ILoginProps { };
export interface ILoginSates {
    name: string;
    pass: string;
    islogin: boolean;
}
export default class Login extends React.Component<ILoginProps, ILoginSates>{
    constructor(props: ILoginProps) {
        super(props);
        this.state = {
            name: "",
            pass: "",
            islogin: false
        }
        this.login = this.login.bind(this);
    }
    public checkInput(name: any, pass: any) {
        let check = true;;
        if (name === null || name.trim() === '') {
            $('#username-error').html('username null')
            check = false
        }
        if (pass === null || pass.trim() === '') {
            $('#password-error').html('password null')
            check = false
        }
        return check;

    }
    public login() {
        const name = ($('#username') as any).val();
        const pass = ($('#password') as any).val();
          console.log(name,pass);
        
        request.get(`http://localhost:3001/user`, {
        
        qs: {
                name: name,
                pass: pass
            }
        }, (error, response, body) => {
            if(error){
                console.log(error);
                $('#login-status').html('<h2>'+error+'<h2>')
            }else {
                console.log(body);
                const user = JSON.parse(body);
                if(user[0]){
                    
                    $('#login-status').html('<h2>xin chao:'+user[0].lastname+'<h2>');
                }
                else{
                    $('#login-status').html('<h2>username hoặc mật khẩu sai<h2>');
                }
            }
        });


    }
    public render() {
        return (
            <div>
                <div className="container-faulier">
                    <div> <b>UserName:</b> <input type="text" id="username" placeholder="Enter userName" /></div>
                    <div id="username-error" />
                    <div> <b>Password:</b> <input type="text" id="password" placeholder="Enter password" />  </div>
                    <div id="password-error" />
                    <div id="login-status" />

                    <button id="button-login" type="button" onClick={this.login}>Login</button>

                </div>
            </div>
        )
    }
} 