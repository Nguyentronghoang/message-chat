import * as React from 'react';
interface INotifiProps {
    species: string,
    notifi: string,
}
interface INotifiStates {
    numOfNoti: number;
}
export default class Notification extends React.Component<INotifiProps, INotifiStates>{
    constructor(props: INotifiProps) {
        super(props);
        this.state = {
            numOfNoti: 0,
        }

    }
    public render() {
        return (
            <div className="notification-container">
                <a className="notification-header" href="#"><h3>Notification</h3> <span className="numberNotification">{this.state.numOfNoti}</span></a>

                <div className="notification-main">
                    <div className="special-noti"><h5>{this.props.species}</h5></div>
                    <div className="conten-noti">{this.props.notifi}</div>
                </div>
            </div>
        )
    }
}