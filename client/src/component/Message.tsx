import * as React from 'react';
export interface IShowMessageProps{
    username: string;
    text: string;
}
export interface IShowMessageSates{
   
}
export default class MessageShow extends React.Component<IShowMessageProps,IShowMessageSates> {
    constructor(props:IShowMessageProps){
        super(props);
    }
    public render() {
        return (
            <div className="message">
            <b>{this.props.username}</b> : {this.props.text}
            </div>
        )
    }
}