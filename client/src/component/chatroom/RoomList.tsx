import * as React from 'react';
interface IRoomListProps{
    rooms: any[];
    roomId:string; 
    subscribeToRoom:(roomId:string)=>void  
}
interface IRoomListStates{

}
export default class RoomList extends React.Component<IRoomListProps,IRoomListStates> {
    constructor(props: IRoomListProps){
        super(props);
        this.subscribeToRoom= this.subscribeToRoom.bind(this);
    }
    public subscribeToRoom(){
        this.props.subscribeToRoom(this.props.roomId);
    }

   public render () {
        const orderedRooms = [...this.props.rooms].sort();
        return (
            <div className="rooms-list">
                <ul>
                <h3>Your rooms:</h3>
                    {orderedRooms.map(room => {
                        const active = room.id === this.props.roomId ? 'active' : '';
                        return (
                            <li key={room.id} className={"room " + active}>
                                <a
                                    onClick={this.subscribeToRoom}
                                    href="#">
                                    # {room.name}
                                </a>
                            </li>
                        )
                    })}
                </ul>
            </div>
        )
    }
}