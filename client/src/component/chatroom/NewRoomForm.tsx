import * as React from 'react';
interface INewRoomFormProps{
    createRoom:(room: string)=>void;
}
interface INewRoomFormStates{
    roomName: string;
}
export default class NewRoomForm extends React.Component<INewRoomFormProps,INewRoomFormStates>{
    constructor(props: INewRoomFormProps){
        super(props);
        this.state={
            roomName:''
        }
        this.handleChange = this.handleChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }
    public handleChange(e:any){
            this.setState({
                roomName: e.target.value
            })
    }
    public handleSubmit(e:any){
        e.preventDefault()
        this.props.createRoom(this.state.roomName)
        this.setState({roomName: ''})
    }
    public render(){
        return(
            <div className="new-room-Form">
            <form onSubmit={this.handleSubmit} >
                    <input
                        value={this.state.roomName}
                        onChange={this.handleChange}
                        type="text" 
                        placeholder="Create a room" 
                        required />
                    <button id="create-room-btn" type="submit">+</button>
            </form>
            </div>
        )
    }
}