import * as React from 'react'
import './chatroom.css';
export interface IChatroomProps {
    chatroom: string;
    join: (roomchat: string) => void;
}
export interface IChatroomStates {

}
export default class Chatroom extends React.Component<IChatroomProps, IChatroomStates>{
    constructor(props: IChatroomProps) {
        super(props);
        this.join = this.join.bind(this);
       
    }
  
    public render() {
        return (
            <div className="room">
                {/* <li>
                    <span className="message">
                    <span className="count">1</span>
                    </span>
                    <img src="/assets/images/portraits/1.jpg" alt=""/>
                    <span className="name">{this.props.chatroom}
                    <span className="status">Active</span>
                    </span> */}
                <button className="butoon-room" type="button" onClick={this.join}><p><img className="img-responsive" src="/assets/icons/default.png" alt="..." /> <b>{this.props.chatroom}</b> </p></button>
                {/* </li> */}
            </div>
        )
    }
    private join() {
        const chatroom = this.props.chatroom;
        this.props.join(chatroom);
    }
    
}