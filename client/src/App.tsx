import * as React from 'react';
import './App.css';
export default class App extends React.Component {
    public render() {
        return (
            <div>
                <div className="container">
                    <div className="app">
                        <div className="head clearfix">
                            <span className="message-notification">
                                <i className="fa fa-comment-o" />
                                <span className="count">2</span>
                            </span>
                            <span className="title">Messager</span>
                            <span className="create-new">
                                <i className="fa fa-pencil-square-o" />
                            </span>
                        </div>
                        <div className="search-bar">
                            <i className="fa fa-search" />
                            <input type="text" className="ip-search" placeholder="Search" />
                        </div>
                        <div className="body">
                            <div className="friend-list">
                                <ul>
                                    <li>
                                      span.messages  
                                    </li> 
                                    </ul>
                            </div></div>
                    </div>
                </div>

            </div>
        )
    }
}
