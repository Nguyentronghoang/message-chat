Cài đặt
Bước 1: clone code về
Bước 2: mở file bằng visual code
Bước 3: cài thư viện: mở file client chạy npm install để cài đặt thư viện cho client.  mở file server , chạy npm install để cài thư viện cho server
Bước 4: chạy client và server: ở client: npm start và ở server: npm start . Client sẽ khởi động cổng 3000, server cổng 3001
Bước 5: test chương trình
  -Mở trình duyệt theo đường link : http://localhost:3000, mở hai tab
  -Nhập tên và phòng: click vào let me chat để bắt đầu
  -Nhập tin nhắn vào và ấn send để gửi
  
  Chú ý:  tên trống, hoặc đã được nhập sẽ báo lỗi cần nhập lại
       -Phòng nếu chưa được tạo sẽ tự động được tạo
       -Vào cùng phòng có thể chat cùng nhau
      